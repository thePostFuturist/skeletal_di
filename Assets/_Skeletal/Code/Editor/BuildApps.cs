using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;


public class BuildApps : EditorWindow {

	static string[] GetNormalLevels()
	{
		string[] levels = {
			"Assets/Celgene/Scenes/Splash1.unity",
			"Assets/Celgene/Scenes/Scene1.unity",
			"Assets/Celgene/Scenes/IntroV3.unity",
			"Assets/Celgene/Scenes/ScenesAbstract/aOffice.unity",
			"Assets/Celgene/Scenes/ScenesAbstract/RootDCR.unity",
			"Assets/Celgene/Scenes/ScenesAbstract/HyperdriveDCR.unity",
			"Assets/Celgene/Scenes/ScenesAbstract/aFarLand.unity",
			"Assets/Celgene/Scenes/ScenesAbstract/OfficeDCR.unity",
			"Assets/Celgene/Scenes/ScenesAbstract/aDoctor.unity",
			"Assets/Celgene-2/Safety_Gateway.unity",
			"Assets/Celgene-2/Payments.unity",
			"Assets/Celgene-2/Trial_Management-2.unity",
			"Assets/Celgene-2/ePro.unity"
		};
		return levels;
	}

	static string[] GetGenericLevels()
	{
		string[] levels = {
			"Assets/Celgene/Scenes/Splash1Generic.unity",
			"Assets/Celgene/Scenes/Scene1Generic.unity",
			"Assets/Celgene/Scenes/IntroV3Generic.unity",
			"Assets/Celgene/Scenes/ScenesAbstract/aOfficeGeneric.unity",
			"Assets/Celgene/Scenes/ScenesAbstract/RootDCR.unity",
			"Assets/Celgene/Scenes/ScenesAbstract/HyperdriveDCR.unity",
			"Assets/Celgene/Scenes/ScenesAbstract/aFarLandGeneric.unity",
			"Assets/Celgene/Scenes/ScenesAbstract/OfficeDCRGeneric.unity",
			"Assets/Celgene/Scenes/ScenesAbstract/aDoctor.unity",
			"Assets/Celgene-2/Safety_GatewayGeneric.unity",
			"Assets/Celgene-2/PaymentsGeneric.unity",
			"Assets/Celgene-2/Trial_Management-2Generic.unity",
			"Assets/Celgene-2/eProGeneric.unity"
		};
		return levels;
	}

	[MenuItem("Build/Set to Normal")]
	static void SetToNormal()
	{
		string[] levels = GetNormalLevels();
		List<EditorBuildSettingsScene> editorBuildSettingsScenes = new List<EditorBuildSettingsScene>();
        foreach (string scenePath in levels)
        {
            if (!string.IsNullOrEmpty(scenePath))
                editorBuildSettingsScenes.Add(new EditorBuildSettingsScene(scenePath, true));
        }
        // Set the Build Settings window Scene list
        EditorBuildSettings.scenes = editorBuildSettingsScenes.ToArray();
		PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS, "");
		PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, "");
	}

	[MenuItem("Build/Set to Generic")]
	static void SetToGeneric()
	{
		string[] levels = GetGenericLevels();
		List<EditorBuildSettingsScene> editorBuildSettingsScenes = new List<EditorBuildSettingsScene>();
        foreach (string scenePath in levels)
        {
            if (!string.IsNullOrEmpty(scenePath))
                editorBuildSettingsScenes.Add(new EditorBuildSettingsScene(scenePath, true));
        }
        // Set the Build Settings window Scene list
        EditorBuildSettings.scenes = editorBuildSettingsScenes.ToArray();
		PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS, "_GENERIC");
		PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, "_GENERIC");
	}


	[MenuItem("Build/Android/Normal/Run")]
	static void BuildAndroidNormalRun()
	{
		BuildAndroidNormal(true);
	}

	[MenuItem("Build/Android/Normal/Build only")]
	static void BuildAndroidNormalBuild()
	{
		BuildAndroidNormal(false);
	}

	static void BuildAndroidNormal(bool run)
	{
        Debug.Log ("Building for Android, Normal");
		string[] levels = GetNormalLevels();
		PlayerSettings.productName = "CelgeneVR";
		PlayerSettings.applicationIdentifier = "com.accenture.celgenevr";
		PlayerSettings.virtualRealitySupported = false;
		BuildPipeline.BuildPlayer( levels, "App/CelgeneVR.apk", BuildTarget.Android, run ? BuildOptions.AutoRunPlayer : BuildOptions.None); 
		EditorApplication.Beep();
	}

	[MenuItem("Build/iOS/Normal/Run")]
	static void BuildiOSNormalRun()
	{
		BuildiOSNormal(true);
	}

	[MenuItem("Build/iOS/Normal/Build only")]
	static void BuildiOSNormalBuild()
	{
		BuildiOSNormal(false);
	}

	static void BuildiOSNormal(bool run)
	{
        Debug.Log ("Building for iOS, Normal");
		string[] levels = GetNormalLevels();
		PlayerSettings.productName = "CelgeneVR";
		PlayerSettings.applicationIdentifier = "com.accenture.celgenevr";
		PlayerSettings.virtualRealitySupported = false;
		BuildPipeline.BuildPlayer( levels, "App/CelgeneVR.apk", BuildTarget.iOS, run ? BuildOptions.AutoRunPlayer : BuildOptions.None); 
		EditorApplication.Beep();
	}

	[MenuItem("Build/Android/Generic/Run")]
	static void BuildAndroidGenericRun()
	{
		BuildAndroidGeneric(true);
	}

	[MenuItem("Build/Android/Generic/Build only")]
	static void BuildAndroidGenericBuild()
	{
		BuildAndroidGeneric(false);
	}

	static void BuildAndroidGeneric(bool run)
	{
		Debug.Log ("Building for Android, Generic");
		string[] levels = GetGenericLevels();
		PlayerSettings.productName = "PharmaVR";
		PlayerSettings.applicationIdentifier = "com.accenture.pharmavr";
		PlayerSettings.virtualRealitySupported = false;
		PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, "_GENERIC");
		BuildPipeline.BuildPlayer( levels, "App/PharmaVR.apk", BuildTarget.Android, run ? BuildOptions.AutoRunPlayer : BuildOptions.None); 
		EditorApplication.Beep();
	}

	[MenuItem("Build/iOS/Generic/Run")]
	static void BuildiOSGenericRun()
	{
		BuildiOSGeneric(true);
	}

	[MenuItem("Build/iOS/Generic/Build only")]
	static void BuildiOSGenericBuild()
	{
		BuildiOSGeneric(false);
	}

	static void BuildiOSGeneric(bool run)
	{
        Debug.Log ("Building for iOS, Generic");
		string[] levels = GetGenericLevels();
		PlayerSettings.productName = "PharmaVR";
		PlayerSettings.applicationIdentifier = "com.accenture.pharmavr";
		PlayerSettings.virtualRealitySupported = false;
		PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS, "_GENERIC");
		BuildPipeline.BuildPlayer( levels, "App/PharmaVR", BuildTarget.iOS, run ? BuildOptions.AutoRunPlayer : BuildOptions.None); 
		EditorApplication.Beep();
	}

/*
	[MenuItem("Build/CoE/CoE iOS")]
	static void BuildCoEiOS()
	{
		Debug.Log ("Building CoE for iOS");
		AssetDatabase.MoveAsset("Assets/StreamingAssetsCoE/VSVEIndiaKannanSundaresanV1.3gp", "Assets/StreamingAssets/VSVEIndiaKannanSundaresanV1.3gp");
		string[] levels = GetCoELevels();
		PlayerSettings.productName = "CoE Panoramas";
		PlayerSettings.bundleIdentifier = "com.accenture.coepanoramictour";
		PlayerSettings.virtualRealitySupported = false;
		BuildPipeline.BuildPlayer( levels, "Builds/CoEPanoramas", BuildTarget.iOS, BuildOptions.AutoRunPlayer); 
		AssetDatabase.MoveAsset("Assets/StreamingAssets/VSVEIndiaKannanSundaresanV1.3gp", "Assets/StreamingAssetsCoE/VSVEIndiaKannanSundaresanV1.3gp");
		EditorApplication.Beep();
	}

	[MenuItem("Build/LAS/Android/Run")]
	static void BuildLASAndroidRun()
	{
		BuildLASAndroid (true);
	}
	[MenuItem("Build/LAS/Android/Build")]
	static void BuildLASAndroidBuild()
	{
		BuildLASAndroid (false);
	}

	static void BuildLASAndroid(bool run)
	{
		Debug.Log ("Building LAS for Android");
		AssetDatabase.MoveAsset("Assets/StreamingAssetsLAS/Intro.3gp", "Assets/StreamingAssets/Intro.3gp");
		string[] levels = GetLASLevels();
		PlayerSettings.productName = "LAS Panoramas";
		PlayerSettings.bundleIdentifier = "com.accenture.laspanoramictour";
		PlayerSettings.virtualRealitySupported = false;
		BuildPipeline.BuildPlayer( levels, "Builds/LASPanoramas.apk", BuildTarget.Android, run ? BuildOptions.AutoRunPlayer : BuildOptions.None); 
		AssetDatabase.MoveAsset("Assets/StreamingAssets/Intro.3gp", "Assets/StreamingAssetsLAS/Intro.3gp");
		EditorApplication.Beep();
	}

	[MenuItem("Build/LAS/Windows/Run")]
	static void BuildLASWindows(bool run)
	{
		Debug.Log ("Building LAS for Windows");
		AssetDatabase.MoveAsset("Assets/StreamingAssetsLAS/Intro.ogv", "Assets/Resources/Intro.ogv");
		string[] levels = GetLASLevels();
		PlayerSettings.productName = "LAS Panoramas";
		PlayerSettings.bundleIdentifier = "com.accenture.laspanoramictour";
		PlayerSettings.virtualRealitySupported = true;
		BuildPipeline.BuildPlayer( levels, "Builds/LASPanoramas.exe", BuildTarget.StandaloneWindows64, run ? BuildOptions.AutoRunPlayer : BuildOptions.None); 
		PlayerSettings.virtualRealitySupported = false;
		AssetDatabase.MoveAsset("Assets/Resources/Intro.ogv", "Assets/StreamingAssetsLAS/Intro.ogv");
		EditorApplication.Beep();
	}

	[MenuItem("Build/LAS/LAS iOS")]
	static void BuildLASiOS()
	{
		Debug.Log ("Building CoE for iOS");
		AssetDatabase.MoveAsset("Assets/StreamingAssetsLAS/Intro.3gp", "Assets/StreamingAssets/Intro.3gp");
		string[] levels = GetLASLevels();
		PlayerSettings.productName = "LAS Panoramas";
		PlayerSettings.bundleIdentifier = "com.accenture.laspanoramictour";
		BuildPipeline.BuildPlayer( levels, "Builds/LASPanoramas", BuildTarget.iOS, BuildOptions.AutoRunPlayer); 
		AssetDatabase.MoveAsset("Assets/StreamingAssets/Intro.3gp", "Assets/StreamingAssetsLAS/Intro.3gp");
		EditorApplication.Beep();
	}

	[MenuItem("Build/All/Android/Build only")]
	static void BuildAllAndroidBuild()
	{
		BuildAll(BuildTarget.Android, false, "Builds/Panoramas.apk");
	}

	[MenuItem("Build/All/iOS/Build only")]
	static void BuildAlliOSBuild()
	{
		BuildAll(BuildTarget.iOS, false, "Builds/Panoramas");
	}

	[MenuItem("Build/All/Android/And Run")]
	static void BuildAllAndroidRun()
	{
		BuildAll(BuildTarget.Android, true, "Builds/Panoramas.apk");
	}
	[MenuItem("Build/All Android Apps")]
	static void BuildAllAndroids()
	{
		BuildCoEAndroid(false);
		BuildLASAndroid(false);
		BuildAllAndroidBuild();
	}

	[MenuItem("Build/All/WebGL/And Run")]
	static void BuildAllWebGLRun()
	{
		BuildAll(BuildTarget.WebGL, true, "Builds/WebGLPanoramas");
	}
*/

}
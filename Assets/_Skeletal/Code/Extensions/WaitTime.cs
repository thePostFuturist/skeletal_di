﻿//Copyright 2019 Valentin Burov
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


using System;
using System.Collections;
using UnityEngine;

public class WaitTime
{
	/// <summary>
	///     Waits a specified amount of time, then invokes the action on completion
	/// </summary>
	/// <param name="m_callback"></param>
	/// <param name="m_time"></param>
	/// <returns></returns>
	public static IEnumerator Wait(Action m_callback, float m_time)
    {
        yield return new WaitForSeconds(m_time);
        m_callback();
    }

    public static IEnumerator Wait(float m_time, Action m_callback)
    {
        yield return new WaitForSeconds(m_time);
        m_callback();
    }

    public static IEnumerator WaitFrame(Action m_callback)
    {
        yield return new WaitForEndOfFrame();
        m_callback();
    }
}
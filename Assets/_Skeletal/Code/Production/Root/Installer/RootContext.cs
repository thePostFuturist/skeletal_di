using Skeleton.Signals;
using Zenject;

namespace Skeleton
{
    public class RootContext : MonoInstaller<RootContext>
    {
        public override void InstallBindings()
        {
            SignalBusInstaller.Install(Container);

            Container.DeclareSignal<TimelineDirectorOnCallSignal>();
            Container.DeclareSignal<TimelineButtonSignal>();
            Container.DeclareSignal<PlaybackSignal>();
            Container.DeclareSignal<TapeButtonAnimatorSignal>();
            Container.DeclareSignal<TapeButtonPlaybackSignal>();
            Container.DeclareSignal<ToggleOptionsSignal>();
            Container.DeclareSignal<RestartSignal>();
            Container.DeclareSignal<StopAllTimelinesSignal>();
            Container.DeclareSignal<CompletedTimelineSignal>();
            Container.DeclareSignal<LoadRequestedSceneSignal>();
            Container.DeclareSignal<AllScenesLoaded>();
            Container.Bind<TimelinesPlayed>().AsSingle();
        }
    }
}
﻿using System;
using System.Collections;
using Skeleton.Signals;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace Skeleton
{
    public class SceneLoadManager : MonoBehaviour, ISceneLoadManagerView
    {
        private Action
            callback; // null callback so we just pass of a blank action instead of writing it out. Not worth the writing to create an overloaded Signal


        [SerializeField] private string scene_directory;

        private string scene_name_full_string;
        
        [Inject] private SignalBus signalBus;


        [Inject] private ZenjectSceneLoader zenjectSceneLoader;

        public void ManageScene(SceneNames scene_name, bool state, Action _callback)
        {
            scene_name_full_string = scene_directory + scene_name;

            if (state)
            {
                callback = _callback;
                LoadScene(scene_name);
            }
            else
            {
                UnloadScene(scene_name);
            }
        }

        [Inject]
        public void Subscribe()
        {
            signalBus.Subscribe(delegate(LoadRequestedSceneSignal _signal)
            {
                ManageScene(_signal.sceneNames, _signal.shouldLoad, _signal.callback);
            });
        }

        private IEnumerator LoadRoutine(string scene_name_full_string)
        {
            float starting_time = Time.time;
            AsyncOperation op = zenjectSceneLoader.LoadSceneAsync(scene_name_full_string, LoadSceneMode.Additive);
////            AsyncOperation op = SceneManager.LoadSceneAsync(scene_name_full_string, LoadSceneMode.Additive);
            op.allowSceneActivation = false;

            Debug.Log("Loading: " + scene_name_full_string);

            while (!op.isDone)
            {
                if (!op.progress.CompareFloats(1f))
                {
                    op.allowSceneActivation = true;
                }

                yield return null;
            }

            Debug.Log("Finished Loading " + scene_name_full_string + "\n    Time spent: " +
                      (Time.time - starting_time) + " seconds");
            if (callback != null)
            {
                callback();
            }
        }

        private void LoadScene(SceneNames scene_name)
        {
            StartCoroutine(LoadRoutine(scene_name_full_string));
        }

        private void UnloadScene(SceneNames scene_name)
        {
            SceneManager.UnloadSceneAsync(scene_name_full_string);
        }

        private void Reboot()
        {
            zenjectSceneLoader.LoadScene(scene_directory + SceneNames.Root, LoadSceneMode.Single);
        }
    }

    public interface ISceneLoadManagerView
    {
        void ManageScene(SceneNames scene_name, bool state, Action _callback);
    }
}
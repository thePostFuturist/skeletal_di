﻿using System;
using Skeleton.Signals;
using UnityEngine;
using Zenject;

namespace Skeleton
{
    public enum SceneNames
    {
        UI,
        UI_lab,
        World,
        World_lab,
        Util,
        All_lab,
        All,
        Root,
        All_VR,
        WorldVR,
        UIVR
    }

    public class LoadInitialScenes : MonoBehaviour
    {
        private readonly Action null_callback = () =>
        {
        };

        [Inject] private SignalBus signalBus;


        [SerializeField] private SceneNames to_load = SceneNames.All;

        private void Start()
        {
            LoadTheseScenesOnInit();
        }


        private void LoadTheseScenesOnInit()
        {
            if (to_load == SceneNames.All_lab)
            {
                Action load_util = () =>
                {
//                    DispatchLoad(SceneNames.Util, true, null_callback);
                    DispatchAllScenesLoaded(true);
                };

                Action load_world = () =>
                {
                    DispatchLoad(SceneNames.World_lab, true, load_util);
                };

                Action load_ui = () =>
                {
                    DispatchLoad(SceneNames.UI_lab, true, load_world);
                };

                load_ui();
            }
            else if (to_load == SceneNames.All)
            {
                Action load_util = () =>
                {
//                  DispatchLoad(SceneNames.Util, true, null_callback);
                    DispatchAllScenesLoaded(true);
                };

                Action load_world = () =>
                {
                    DispatchLoad(SceneNames.World, true, load_util);
                };

                Action load_ui = () =>
                {
                    DispatchLoad(SceneNames.UI, true, load_world);
                };

                load_ui();
            }
            
            else if (to_load == SceneNames.All_VR)
            {
                Action load_util = () =>
                {
//                  DispatchLoad(SceneNames.Util, true, null_callback);
                    DispatchAllScenesLoaded(true);
                };

                Action load_world = () =>
                {
                    DispatchLoad(SceneNames.WorldVR, true, load_util);
                };

                Action load_ui = () =>
                {
                    DispatchLoad(SceneNames.UIVR, true, load_world);
                };

                load_ui();
            }
       
            else
            {
                DispatchLoad(to_load, true, null_callback);
            }
        }

        private void DispatchLoad(SceneNames _sceneName, bool _state, Action _callback)
        {
            signalBus.Fire(new LoadRequestedSceneSignal
            {
                callback = _callback,
                sceneNames = _sceneName,
                shouldLoad = _state
            });
        }
        
        void DispatchAllScenesLoaded (bool _areAllScenesLoaded)
        {
            signalBus.Fire(new AllScenesLoaded
            {
               _scenes_loaded = _areAllScenesLoaded
            });
        }
    }
}
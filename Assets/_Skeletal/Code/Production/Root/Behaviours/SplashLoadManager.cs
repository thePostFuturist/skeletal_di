﻿using System.Collections;
using BitStrap;
using Skeleton.Signals;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class SplashLoadManager : MonoBehaviour
{
    [SerializeField] [ReadOnly] private Image logoObject;

    [SerializeField] private string logoObjectName = "LogoImage";
    [SerializeField] private bool shouldZoom;


    [Inject] private SignalBus signalBus;

    [SerializeField] private float speed;
    [SerializeField] [ReadOnly] private Transform splashObject;

    [SerializeField] private string splashObjectName = "Splash";
    [SerializeField] private Vector3 startingScale = Vector3.one, endingScale = Vector3.one;

    [Inject]
    public void Subscribe()
    {
        signalBus.Subscribe(delegate(AllScenesLoaded _signal)
        {
            OnAllScenesLoaded(_signal._scenes_loaded);
        });
    }

    private void Start()
    {
        FindVars();
        if (shouldZoom)
        {
            StartCoroutine(ZoomLogo());
        }
    }

    [Button]
    private void FindVars()
    {
        splashObject = transform.find<Transform>(splashObjectName);
        logoObject = transform.find<Image>(logoObjectName);
    }


    private void OnAllScenesLoaded(bool _allScenesLoaded)
    {
        if (_allScenesLoaded)
        {
            if (shouldZoom)
            {
                StopAllCoroutines();
                splashObject.gameObject.SetActive(false);
            }
        }
        else
        {
            if (shouldZoom)
            {
                splashObject.gameObject.SetActive(true);
                StartCoroutine(ZoomLogo());
            }
        }
    }


    private IEnumerator ZoomLogo()
    {
        float i = 0f;
        while (i < 1f)
        {
            logoObject.transform.localScale = Vector3.Lerp(startingScale, endingScale, i);
            i += speed * Time.deltaTime;
            yield return null;
        }
    }
}
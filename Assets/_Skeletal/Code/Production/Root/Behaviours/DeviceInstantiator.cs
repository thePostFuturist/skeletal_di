﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace Skeleton
{
    public class DeviceInstantiator : MonoBehaviour
    {
        public static DeviceInstantiator deviceSingleton;

        [Inject] protected readonly DiContainer _container;

        [SerializeField] private GameObject inputDevice;

        private void Awake()
        {

            if (deviceSingleton == null)
            {
                CheckSceneCount();
                DontDestroyOnLoad(gameObject);
                deviceSingleton = this;
                InstantiateDevice();
            }
            else if (deviceSingleton != this)
            {
                Destroy(gameObject);
            }
        }

        public void CheckSceneCount()
        {
            if (SceneManager.sceneCount > 1)
            {
                Destroy(gameObject);
            }
        }

        private void InstantiateDevice()
        {
            if (DeviceSingleton.device == null)
            {
                _container.InstantiatePrefab(inputDevice);
            }
        }
    }
}
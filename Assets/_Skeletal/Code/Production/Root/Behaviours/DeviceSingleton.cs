﻿using UnityEngine;

namespace Skeleton
{
    public class DeviceSingleton : MonoBehaviour
    {
        public static DeviceSingleton device;


        private void Awake()
        {
            if (device == null)
            {
                DontDestroyOnLoad(gameObject);
                device = this;
            }
            else if (device != this)
            {
                Destroy(gameObject);
            }
        }
    }
}
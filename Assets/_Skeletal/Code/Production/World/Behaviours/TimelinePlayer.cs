﻿using Skeleton.Signals;
using UnityEngine;
using UnityEngine.Playables;
using Zenject;

namespace Skeleton
{
    [RequireComponent(typeof(PlayableDirector))]
    public class TimelinePlayer : MonoBehaviour
    {
        [Inject] private readonly SignalBus signalBus;


        [SerializeField] private bool is_current_timeline;

        private PlayableDirector playableDirector;


        [SerializeField] private TimelineIndicator timelineIndicatorDirector;

        [Inject]
        private void Register()
        {
            signalBus.Subscribe(delegate(TimelineDirectorOnCallSignal _signal)
            {
                ReceiveTimeline(_signal.timelineIndicator);
            });

            signalBus.Subscribe(delegate(TapeButtonPlaybackSignal _signal)
            {
                OnTapeButtonPlaybackSignal(_signal.tapePlayerButtonIndicator);
            });

            signalBus.Subscribe(delegate(StopAllTimelinesSignal _signal)
            {
                StopDirector();
            });
        }


        private void Start()
        {
            playableDirector = gameObject.EntityComponent<PlayableDirector>();
        }

        private void ReceiveTimeline(TimelineIndicator _timelineIndicatorDirector)
        {
            if (CheckChapterIndicatorMatch(_timelineIndicatorDirector))
            {
                AssignCurrentTimeline(true);
                PlayDirector();
            }
            else
            {
                AssignCurrentTimeline(false);
            }
        }

        private void AssignCurrentTimeline(bool _isCurrentTimeline)
        {
            is_current_timeline = _isCurrentTimeline;
        }

        private void OnTapeButtonPlaybackSignal(TapePlayerButtonIndicator _tape_player_button_indicator)
        {
            if (is_current_timeline == false)
            {
                return;
            }

            switch (_tape_player_button_indicator)
            {
                case TapePlayerButtonIndicator.Stop:
                    StopDirector();
                    break;
                case TapePlayerButtonIndicator.Play:
                    PlayDirector();
                    break;
                case TapePlayerButtonIndicator.Pause:
                    PauseDirector();
                    break;
                case TapePlayerButtonIndicator.Rewind:
                    RewindDirector();
                    break;
            }
        }


        private bool CheckChapterIndicatorMatch(TimelineIndicator _timelineIndicatorDirector)
        {
            if (_timelineIndicatorDirector == timelineIndicatorDirector)
            {
                return true;
            }

            return false;
        }

        private void PlayDirector()
        {
            playableDirector.Play();
            playableDirector.playableGraph.GetRootPlayable(0).SetSpeed(1);
        }

        private void StopDirector()
        {
            RewindDirector();
            PauseDirector();
        }

        private void PauseDirector()
        {
            playableDirector.playableGraph.GetRootPlayable(0).SetSpeed(0);
        }

        private void RewindDirector()
        {
            playableDirector.time = 0f;
            playableDirector.Evaluate();
        }
    }
}
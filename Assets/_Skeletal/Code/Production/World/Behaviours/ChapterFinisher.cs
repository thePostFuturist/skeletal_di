﻿using Skeleton.Signals;
using UnityEngine;
using Zenject;

namespace Skeleton
{
    public class ChapterFinisher : MonoBehaviour
    {
        [Inject] private SignalBus signalBus;

        [SerializeField] private TimelineIndicator whichTimeline;


        private void OnEnable()
        {
            signalBus.Fire(new CompletedTimelineSignal
            {
                timelineIndicator = whichTimeline
            });
        }
    }
}
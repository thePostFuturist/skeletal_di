﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BitStrap;
using Skeleton.Signals;
using Zenject;

namespace Skeleton
{
	public class Restart : MonoBehaviour
	{
//		Signals.RestartSignal restart_signal;
//
//		[Inject]
//		public void Construct(Signals.RestartSignal _restart_signal)
//		{
//			restart_signal = _restart_signal;
//		}
//		
		
		[Inject]
		SignalBus signalBus;
		
		private TapToFireEvent button;
		
		private void Start()
		{
			FindVars();
			Subscribe();
		}

		void FindVars()
		{
			button = gameObject.EntityComponent<TapToFireEvent>();
		}

		void Subscribe()
		{
			button.OnTap.Register(DispatchRestart);
		}


		void DispatchRestart()
		{
			signalBus.Fire(new RestartSignal {} );
		}
	}
}
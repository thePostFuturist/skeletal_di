﻿using UnityEngine;

namespace Skeleton
{
    public enum TimelineIndicator
    {
        Timeline_A,
        Timeline_B,
        Timeline_C,
        Timeline_D,
        Credits
    }

    public class TimelineClassAgnostic : MonoBehaviour
    {
        [SerializeField] public TimelineIndicator timelineIndicator;
    }
}
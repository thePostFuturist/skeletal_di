﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Skeleton
{

	public enum TimelinePartIndicator
	{
		PartA,
		PartB,
		PartC,
		PartD
	}

	public class ChapterPartIndicatorClassAgnostic : MonoBehaviour
	{
		public TimelinePartIndicator timelinePartIndicator;
		
	}

}
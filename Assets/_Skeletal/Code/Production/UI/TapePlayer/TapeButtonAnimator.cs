﻿using Skeleton.Signals;
using UnityEngine;
using Zenject;

namespace Skeleton
{
	/// <summary>
	///     Receives TapeButtonPlayback Signal and Plays corresponding animation
	///     Check the Animator params and the animator structure.
	///     Idle: 0
	///     FocusOn: 1
	///     FocusOff: 2
	///     Tapped: 3
	///     Dim: 4
	///     Disable: 5
	/// </summary>
	[RequireComponent(typeof(Animator))]
    public class TapeButtonAnimator : MonoBehaviour
    {
        [SerializeField] private readonly string animator_parameter_control = "State";


        private Animator _button_animator;
//        private PlaybackSignal play_back_signal;
//        private TapeButtonAnimatorSignal tape_button_animator_signal;

        [Header("This will correspond with Animator:")] [Header("Verify Animator State Param!")] [SerializeField]
        private TapePlayerButtonIndicator tape_player_button_indicator_animator;

        private Animator button_animator
        {
            get
            {
                if (_button_animator == null)
                {
                    _button_animator = gameObject.EntityComponent<Animator>();
                }

                return _button_animator;
            }
        }

//        [Inject]
//        public void Construct(TapeButtonAnimatorSignal _tape_button_animator_signal, PlaybackSignal _play_back_signal)
//        {
//            tape_button_animator_signal = _tape_button_animator_signal;
//            play_back_signal = _play_back_signal;
//        }

        [Inject]
        SignalBus signalBus;
	    /// <summary>
	    ///     Subscribes to ChapterOnCall Signal and its ChapterIndicator param
	    /// </summary>
	    [Inject]
        private void Subscribe()
        {
            signalBus.Subscribe<TapeButtonAnimatorSignal>(delegate(TapeButtonAnimatorSignal _signal)
            {
                ReceiveTapePlayerButtonAnimationState(_signal.tapePlayerButtonIndicator, _signal.whichState);
            });
            
            signalBus.Subscribe<PlaybackSignal>(delegate(PlaybackSignal _signal)
            {
                if (_signal.shouldPlay==true)
                    PlayCorrespondingAnimation(0);
            }); 
            
            //            play_back_signal.Listen(state =>
//            {
//                if (true)
//                {
//                    PlayCorrespondingAnimation(0);
//                }
//            });
        }

        private void Start()
        {
            CheckAnimatorForStateParameter();
        }


        private void CheckAnimatorForStateParameter()
        {
            if (AnimatorParameterShotgunDebugger.HasParameter("State", button_animator) == false)
            {
                Debug.Log("This Animator does not Implement State integer parameter!");
            }
        }

        private void ReceiveTapePlayerButtonAnimationState(TapePlayerButtonIndicator _tape_player_button_indicator,
            int _animation_state)
        {
            // if this animator dismatches the timeline of the button
            if (CheckChapterIndicatorMatch(_tape_player_button_indicator) == false)
            {
                // on focus exit of another button, or on tap of another button
                // these all transition from the dim state
                if (_animation_state == 2 || _animation_state == 3)
                {
                    PlayCorrespondingAnimation(_animation_state);
                }
                // another button on focus on
                else if (_animation_state == 1)
                {
                    PlayCorrespondingAnimation(4); // 4 is the dim state
                }
            }
            // if not, play the appropriate animation state
            else
            {
                PlayCorrespondingAnimation(_animation_state);
            }
        }

        private bool CheckChapterIndicatorMatch(TapePlayerButtonIndicator _tape_player_button_indicator)
        {
            if (_tape_player_button_indicator == tape_player_button_indicator_animator)
            {
                return true;
            }

            return false;
        }

        private void PlayCorrespondingAnimation(int animation_state_determined)
        {
            button_animator.SetInteger(animator_parameter_control, animation_state_determined);
        }
    }
}
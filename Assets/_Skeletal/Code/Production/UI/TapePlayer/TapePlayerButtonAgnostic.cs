﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Skeleton
{
	public enum TapePlayerButtonIndicator
	{
		Play,
		Rewind,
		Stop,
		Pause
	}

	public enum TapePlayerButtonIndicatorDup
	{
		Begin,
		Restart,
		Terminate,
		Rest
	}

	public class TapePlayerButtonAgnostic : MonoBehaviour
	{

		[SerializeField]
		public TapePlayerButtonIndicator timeline_indicator;
		

	}
}

﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

namespace Skeleton
{
    /// <summary>
    ///     toggles the tape buttons
    /// </summary>
    public class Presence : TapToFireEvent
    {
        private List<Image> menuImages = new List<Image>();
        private List<Text> menuTexts = new List<Text>();
        
        bool isOn;

        public override void Start()
        {
            base.Start();


            FindVars();
            Subscribe();
        }

        private void Subscribe()
        {
            OnTap.Register(OnTapListener);
        }

        private void OnEnable()
        {
            ToggleImages(false);
        }


        private void ToggleImages(bool _state)
        {
            foreach (Image menuImage in menuImages)
            {
                if (menuImage.name != "Presence")
                {
                    menuImage.enabled = _state;
                }
            }

            foreach (Text menuText in menuTexts)
            {
                menuText.enabled = _state;
            }
        }

        public override void FindVars()
        {
            base.FindVars();
            menuImages = transform.parent.GetComponentsInChildren<Image>().ToList();
            menuTexts = transform.parent.GetComponentsInChildren<Text>().ToList();
        }


//        private void OnFocusEnter()
//        {
//            ToggleImages(true);
//        }

        private void OnTapListener()
        {
            isOn = !isOn;
//            base.ResetElement(); // for mobile devices. if the user taps on the eye and presses, focus out is never called and the highlight remains on focus enter
            ToggleImages(isOn);
        }
    }
}
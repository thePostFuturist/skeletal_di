﻿using BitStrap;
using Skeleton.Signals;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Skeleton
{
    public enum ButtonType
    {
        CanvasRenderer,
        MeshRenderer,
        None
    }

    [RequireComponent(typeof(Collider))]
    /// <summary>
    /// Check the Animator params and the animator structure. 
    /// 
    /// Idle: 0
    /// FocusOn: 1
    /// FocusOff: 2
    /// Tapped: 3
    /// Dim: 4
    /// Disable: 5
    /// </summary>
    public class TapeButton : TapToFireEvent
    {
        [SerializeField] private readonly float gazeTimeoutAmountOfTime = 10f;

        [SerializeField] [ReadOnly] private ButtonType buttonType = ButtonType.None;

        [SerializeField] private bool isInPlaybackMode;

        [Inject] private SignalBus signalBus;

        [Header("This will correspond with Animator:")] [SerializeField]
        private TapePlayerButtonIndicator tapePlayerButtonIndicator;

        public void Tap()
        {
            DetermineButtonAction();
            GazeTimer(false);
        }

        private void TapEnter()
        {
            DispatchAnimationStateInt(1);
            GazeTimer(true);
        }

        private void TapExit()
        {
            if (isInPlaybackMode)
            {
                DispatchAnimationStateInt(2);
            }

            GazeTimer(false);
        }

        [Inject]
        private void Register()
        {
            signalBus.Subscribe(delegate(PlaybackSignal _signal)
            {
                DetermineAndDecideOnPlaybackStatus(_signal.shouldPlay);
            });

            signalBus.Subscribe(delegate(CompletedTimelineSignal _signal)
            {
                OnFinishedTimeline(_signal.timelineIndicator);
            });
        }


        public override void Start()
        {
            base.Start();
            Subscribe();
            DetermineAndDecideOnPlaybackStatus(isInPlaybackMode);
            DetermineButtonType();
        }

        private void Subscribe()
        {
            onFocusEnter.Register(TapEnter);
            onFocusExit.Register(TapExit);
            OnTap.Register(Tap);
        }

        /// <summary>
        ///     Method to determine the interaction events depending on whether this is a canvas button or just a mesh
        /// </summary>
        private void DetermineButtonType()
        {
            if (gameObject.GetComponent<Button>() != null)
            {
                buttonType = ButtonType.CanvasRenderer;
            }
            else if (gameObject.GetComponent<MeshRenderer>() != null)
            {
                buttonType = ButtonType.MeshRenderer;
            }
            else
            {
                buttonType = ButtonType.None;
            }
        }

        private void OnFinishedTimeline(TimelineIndicator _timelineIndicator)
        {
            ShouldEnableCollider(false);
            DispatchAnimationStateInt(5);
        }

        private void DetermineAndDecideOnPlaybackStatus(bool _isInPlaybackMode)
        {
            isInPlaybackMode = _isInPlaybackMode;

            if (isInPlaybackMode)
            {
                ShouldEnableCollider(true);
                DispatchAnimationStateInt(0);
            }
            else
            {
                ShouldEnableCollider(false);
                DispatchAnimationStateInt(5);
            }
        }

        private void ShouldEnableCollider(bool _shouldEnable)
        {
            buttonCollider.enabled = _shouldEnable;
            
            if (buttonType == ButtonType.CanvasRenderer)
            {
                gameObject.GetComponent<Button>().interactable = _shouldEnable;
            }
        }

        /// <summary>
        ///     Upon gaze, timer starts in case the user doesn't tap on the timeline.
        ///     After wait has elapsed, the same function as tapped as invoked.
        /// </summary>
        /// <param name="_shouldWait"></param>
        private void GazeTimer(bool _shouldWait)
        {
            if (_shouldWait)
            {
                StartCoroutine(WaitTime.Wait(DetermineButtonAction, gazeTimeoutAmountOfTime));
            }
            else
            {
                StopAllCoroutines();
            }
        }

        /// <summary>
        ///     Happens when the button is clicked
        /// </summary>
        private void DetermineButtonAction()
        {
            // stop button also disables colliders on all animations
            if (tapePlayerButtonIndicator == TapePlayerButtonIndicator.Stop)
            {
                DispatchPlaybackSignal(false);
            }

            DispatchTapeButtonPlayback();
        }

        public override void ChangeBackgroundColor(Color _newColor)
        {
            switch (buttonType)
            {
                case ButtonType.CanvasRenderer:
                    gameObject.EntityComponent<Image>().color = _newColor;
                    break;
                case ButtonType.MeshRenderer:
                    gameObject.EntityComponent<MeshRenderer>().material.color = _newColor;
                    break;
            }
        }

        private void DispatchTapeButtonPlayback()
        {
            signalBus.Fire(new TapeButtonPlaybackSignal
            {
                tapePlayerButtonIndicator = tapePlayerButtonIndicator
            });
        }

        private void DispatchPlaybackSignal(bool _shouldPlay)
        {
            signalBus.Fire(new PlaybackSignal
            {
                shouldPlay = _shouldPlay
            });
        }

        /// <param name="_animationStateToDispatch"></param>
        private void DispatchAnimationStateInt(int _animationStateToDispatch)
        {
            signalBus.Fire(new TapeButtonAnimatorSignal
            {
                tapePlayerButtonIndicator = tapePlayerButtonIndicator,
                whichState = _animationStateToDispatch
            });
        }

        private void OnDestroy()
        {
            signalBus.TryUnsubscribe(delegate(PlaybackSignal _signal)
            {
                DetermineAndDecideOnPlaybackStatus(_signal.shouldPlay);
            });

            signalBus.TryUnsubscribe(delegate(CompletedTimelineSignal _signal)
            {
                OnFinishedTimeline(_signal.timelineIndicator);
            });
        }
    }
}
﻿using Skeleton.Signals;
using UnityEngine;
using Zenject;

namespace Skeleton
{
    public class TimelineCanvasButton : TapToFireEvent
    {
        [SerializeField] private bool is_in_playback_mode;
        [Inject] private SignalBus signalBus;

        [Header("This will correspond with Animator:")] [SerializeField]
        private TimelineIndicator timelineIndicatorButton;


        [Inject]
        private void Register()
        {
            signalBus.Subscribe(delegate(PlaybackSignal _signal)
            {
                DetermineAndDecideOnPlaybackStatus(_signal.shouldPlay);
            });
        }

        public override void TapElement()
        {
            base.TapElement();
            DetermineButtonAction();
        }

        private void DispatchChapterPlayback()
        {
            signalBus.Fire(new TimelineDirectorOnCallSignal
            {
                timelineIndicator = timelineIndicatorButton
            });
        }

        private void DispatchAnimationStateInt(int _animation_state_to_dispatch)
        {
            signalBus.Fire(new TimelineButtonSignal
            {
                whichTimelineIndicator = timelineIndicatorButton,
                whichAnimationState = _animation_state_to_dispatch
            });
        }

        private void DetermineAndDecideOnPlaybackStatus(bool _is_in_playback_mode)
        {
            is_in_playback_mode = _is_in_playback_mode;

            if (!is_in_playback_mode)
            {
                base.buttonCollider.enabled = true;
                DispatchAnimationStateInt(0);
                ResetElement();
            }
            else
            {
                base.buttonCollider.enabled = false;
                DispatchAnimationStateInt(5);
                DisableElement();
            }
        }

        private void DetermineButtonAction()
        {
            signalBus.Fire(new PlaybackSignal
            {
                shouldPlay = true
            });
            DispatchChapterPlayback();
        }
    }
}
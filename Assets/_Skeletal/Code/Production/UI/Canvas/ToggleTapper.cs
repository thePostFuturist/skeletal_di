﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BitStrap;

namespace Skeleton
{
	[RequireComponent(typeof(Toggle))]
	public class ToggleTapper : TapToFireEvent
	{
		Toggle toggle;

		[ReadOnly]
		public bool isOn;

		public SafeAction<bool> on_toggle_tap = new SafeAction<bool>();

		public override void Start()
		{
			base.Start();
			toggle = gameObject.EntityComponent<Toggle>();
			isOn = toggle.isOn;
		}

		public override void TapElement()
		{
			base.TapElement();
			ToggleToggler();
		}

		public override void ChangeBackgroundColor(Color new_color)
		{
			toggle.image.color = new_color;
		}

		/// <summary>
		/// This will be accessed in order to determine the toggler state
		/// </summary>
		void ToggleToggler()
		{
			isOn = !isOn;
			toggle.isOn = isOn;
			on_toggle_tap.Call(isOn);
		}
		
	}
}
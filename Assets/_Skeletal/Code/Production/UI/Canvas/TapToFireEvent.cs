﻿using BitStrap;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Skeleton
{
    [RequireComponent(typeof(BoxCollider))]
    public class TapToFireEvent : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        private BoxCollider _buttonCollider;
        [SerializeField] private AudioClip audioFocusEnter, audioFocusExit, audioClick;
        private AudioSource audioSource;

        [ReadOnly] [SerializeField] private bool focused;

        /// <summary>
        /// if youre too lazy to make a collider it will create one for you
        /// </summary>
        [SerializeField] private bool manual_collider = true;

        [SerializeField] private Color normal_color = Color.white,
            highlight_color = Color.blue,
            tapped_color = Color.red,
            disabled_color = Color.white;

        public SafeAction onFocusEnter = new SafeAction();
        public SafeAction onFocusExit = new SafeAction();
        public SafeAction OnTap = new SafeAction();

        [SerializeField] private Vector3 runtime_collider_size = new Vector3(20, 20, 2);

        [HideInInspector]
        public BoxCollider buttonCollider
        {
            get
            {
                if (_buttonCollider == null)
                {
                    _buttonCollider = gameObject.EntityComponent<BoxCollider>();
                }

                return _buttonCollider;
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            OnTap.Call();
            PlaySound(audioClick);
            TapElement();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            focused = true;
            onFocusEnter.Call();
            HighlightElement();
            PlaySound(audioFocusEnter);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            focused = false;
            onFocusExit.Call();
            ResetElement();
        }

        public virtual void Start()
        {
            FindVars();
        }

        private void CreateCollider()
        {
            if (manual_collider == false)
            {
                gameObject.EntityComponent<BoxCollider>().size = runtime_collider_size;
            }
        }

        [Button]
        public virtual void FindVars()
        {
            AssignAudioTriggerEventNames();
            CreateCollider();
        }

        private void AssignAudioTriggerEventNames()
        {
            audioSource = gameObject.EntityComponent<AudioSource>(true);
        }

        public void PlaySound(AudioClip _audioClip)
        {
            audioSource.clip = _audioClip;
            audioSource.Play();
        }

        public void HighlightElement()
        {
            ChangeBackgroundColor(highlight_color);
        }

        public virtual void ResetElement()
        {
            ChangeBackgroundColor(normal_color);
        }

        public virtual void TapElement()
        {
            ChangeBackgroundColor(normal_color);
        }

        public virtual void DisableElement()
        {
            ChangeBackgroundColor(disabled_color);
        }

        public virtual void ChangeBackgroundColor(Color new_color)
        {
            // this will possibly be overridden appropriately by inheriting event
            gameObject.EntityComponent<Image>().color = new_color;
        }
    }
}
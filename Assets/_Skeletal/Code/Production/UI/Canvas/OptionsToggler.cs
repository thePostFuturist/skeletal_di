﻿using Skeleton.Signals;
using UnityEngine;
using Zenject;

namespace Skeleton
{
    [RequireComponent(typeof(TapToFireEvent))]
    public class OptionsToggler : MonoBehaviour
    {
        private TapToFireEvent button;


        [SerializeField] private bool should_turn_on;

        [Inject] private SignalBus signalBus;

        private void Start()
        {
            FindVars();
            Subscribe();
        }

        private void FindVars()
        {
            button = gameObject.EntityComponent<TapToFireEvent>();
        }

        private void Subscribe()
        {
            button.OnTap.Register(DispatchSiestaToggle);
        }

        private void DispatchSiestaToggle()
        {
            signalBus.Fire(new ToggleOptionsSignal
            {
                shouldOpenOptions = should_turn_on
            });
        }
    }
}
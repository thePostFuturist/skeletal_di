﻿using HoloToolkit.Unity;
using Skeleton;
using UnityEngine;

public class CameraToDevice : MonoBehaviour
{
    private void Awake()
    {
        AdjustCam();
    }

    private void AdjustCam()
    {
        CameraCache.cachedCamera = DeviceSingleton.device.GetComponent<Camera>();
    }
}
﻿using UnityEngine;

namespace Skeleton
{
    public class TriggerTapeButton : TapeButton
    {
        private void Start()
        {
            base.Start();
            buttonCollider.isTrigger = true;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<LeverIndicator>() == true)
            {
                Tap();
            }
        }
    }
}
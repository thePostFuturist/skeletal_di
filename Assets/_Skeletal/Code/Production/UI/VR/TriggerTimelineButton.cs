﻿using UnityEngine;

namespace Skeleton
{
    public class TriggerTimelineButton : TimelineButton
    {
        private void Start()
        {
            base.Start();
            button_collider.isTrigger = true;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.GetComponent<LeverIndicator>() == true)
            {
                Debug.Log(345);
                ForceClick();
            }
        }
    }
}
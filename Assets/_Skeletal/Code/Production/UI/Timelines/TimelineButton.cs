﻿using BitStrap;
using Skeleton.Signals;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace Skeleton
{
    [RequireComponent(typeof(BoxCollider))]
    [RequireComponent(typeof(AudioSource))]
    /// <summary>
    ///     Check the Animator params and the animator structure.
    ///     Idle: 0
    ///     FocusOn: 1
    ///     FocusOff: 2
    ///     Tapped: 3
    ///     Dim: 4
    ///     Disable: 5
    /// </summary>
    public class TimelineButton : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private AudioClip audioFocusEnter, audioFocusExit, audioClick;

        private AudioSource audioSource;

        public Collider button_collider;


        [ReadOnly] [SerializeField] private bool focused;

        [SerializeField] private float gaze_timeout_amount_of_time = 5f;

        [SerializeField] private bool is_in_playback_mode;

        [Inject] private SignalBus signalBus;

        [Header("This will correspond with Animator:")] [SerializeField]
        private TimelineIndicator timelineIndicatorButton;

        public void OnPointerClick(PointerEventData eventData)
        {
            DetermineButtonAction();
            GazeTimer(false);
            PlaySound(audioClick);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            focused = true;
            DispatchAnimationStateInt(1);
            GazeTimer(true);
            PlaySound(audioFocusEnter);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            focused = false;
            if (!is_in_playback_mode)
            {
                DispatchAnimationStateInt(2);
            }

            GazeTimer(false);
            PlaySound(audioFocusExit);
        }

        public void ForceClick()
        {
            DetermineButtonAction();
            GazeTimer(false);
            PlaySound(audioClick);
        }

        public void ForceEnter()
        {
            focused = true;
            DispatchAnimationStateInt(1);
            GazeTimer(true);
            PlaySound(audioFocusEnter);
        }

        [Inject]
        private void Register()
        {
            signalBus.Subscribe(delegate(PlaybackSignal _signal)
            {
                DetermineAndDecideOnPlaybackStatus(_signal.shouldPlay);
            });

            signalBus.Subscribe(delegate(CompletedTimelineSignal _signal)
            {
                OnFinishedTimeline(_signal.timelineIndicator);
            });
        }

        public void Start()
        {
            FindVars();
            DetermineAndDecideOnPlaybackStatus(is_in_playback_mode);
        }


        [Button]
        private void FindVars()
        {
            audioSource = gameObject.EntityComponent<AudioSource>(true);
            button_collider = gameObject.EntityComponent<Collider>();
        }

        public void PlaySound(AudioClip _audioClip)
        {
            audioSource.clip = _audioClip;
            audioSource.Play();
        }


        private void OnFinishedTimeline(TimelineIndicator _timelineIndicator)
        {
            button_collider.enabled = true;
            DispatchAnimationStateInt(0);
        }

        private void DetermineAndDecideOnPlaybackStatus(bool _is_in_playback_mode)
        {
            is_in_playback_mode = _is_in_playback_mode;

            if (!is_in_playback_mode)
            {
                button_collider.enabled = true;
                DispatchAnimationStateInt(0);
            }
            else
            {
                button_collider.enabled = false;
                DispatchAnimationStateInt(5);
            }
        }

        /// <summary>
        ///     Upon gaze, timer starts in case the user doesn't tap on the timeline.
        ///     After wait has elapsed, the same function as tapped as invoked.
        /// </summary>
        /// <param name="should_wait"></param>
        private void GazeTimer(bool should_wait)
        {
            if (should_wait)
            {
                StartCoroutine(WaitTime.Wait(DetermineButtonAction, gaze_timeout_amount_of_time));
            }
            else
            {
                StopAllCoroutines();
            }
        }

        private void DetermineButtonAction()
        {
            signalBus.Fire(new PlaybackSignal
            {
                shouldPlay = true
            });
            DispatchTimelinePlayback();
        }

        private void DispatchTimelinePlayback()
        {
            signalBus.Fire(new TimelineDirectorOnCallSignal
            {
                timelineIndicator = timelineIndicatorButton
            });
        }

        private void DispatchPlaybackSignal()
        {
            signalBus.Fire(new PlaybackSignal
            {
                shouldPlay = false
            });
        }

        /// <param name="_animation_state_to_dispatch"></param>
        private void DispatchAnimationStateInt(int _animation_state_to_dispatch)
        {
            signalBus.Fire(new TimelineButtonSignal
            {
                whichTimelineIndicator = timelineIndicatorButton, whichAnimationState = _animation_state_to_dispatch
            });
        }
    }
}
﻿using Skeleton.Signals;
using UnityEngine;
using Zenject;

namespace Skeleton
{
	/// <summary>
	///     Receives the ChapyerButtonSignal and plays corresponding animation
	///     Check the Animator params and the animator structure.
	///     Idle: 0
	///     FocusOn: 1
	///     FocusOff: 2
	///     Tapped: 3
	///     Dim: 4
	///     Disable: 5
	/// </summary>
	[RequireComponent(typeof(Animator))]
    public class TimelineButtonAnimator : MonoBehaviour
    {
        [SerializeField] private readonly string animator_parameter_control = "State";


        private Animator _button_animator;


        [Inject] private SignalBus signalBus;

        [Header("This will correspond with Animator:")] [Header("Verify Animator State Param!")] [SerializeField]
        private TimelineIndicator timelineIndicatorAnimator;

        private Animator button_animator
        {
            get
            {
                if (_button_animator == null)
                {
                    _button_animator = gameObject.EntityComponent<Animator>();
                }

                return _button_animator;
            }
        }

        /// <summary>
        ///     Subscribes to ChapterOnCall Signal and its ChapterIndicator param
        /// </summary>
        [Inject]
        private void Subscribe()
        {
            signalBus.Subscribe(delegate(TimelineButtonSignal _signal)
            {
                ReceiveTimelineAnimationState(_signal.whichTimelineIndicator, _signal.whichAnimationState);
            });
//			timelineButtonSignal.Listen(ReceiveTimelineAnimationState);
        }

        private void Start()
        {
            CheckAnimatorForStateParameter();
        }


        private void CheckAnimatorForStateParameter()
        {
            if (AnimatorParameterShotgunDebugger.HasParameter("State", button_animator) == false)
            {
                Debug.Log("This Animator does not Implement State integer parameter!");
            }
        }

        private void ReceiveTimelineAnimationState(TimelineIndicator _timelineIndicator, int _animation_state)
        {
            // if this animator dismatches the timeline of the button
            if (CheckTimelineIndicatorMatch(_timelineIndicator) == false)
            {
                // on focus exit of another button, or on tap of another button
                // these all transition from the dim state
                if (_animation_state == 2 || _animation_state == 3)
                {
                    PlayCorrespondingAnimation(_animation_state);
                }
                // another button on focus on
                else if (_animation_state == 1)
                {
                    PlayCorrespondingAnimation(4); // 4 is the dim state
                }
            }
            // if not, play the appropriate animation state
            else
            {
                PlayCorrespondingAnimation(_animation_state);
            }
        }

        private bool CheckTimelineIndicatorMatch(TimelineIndicator _timelineIndicator)
        {
            if (_timelineIndicator == timelineIndicatorAnimator)
            {
                return true;
            }

            return false;
        }

        private void PlayCorrespondingAnimation(int animation_state_determined)
        {
            button_animator.SetInteger(animator_parameter_control, animation_state_determined);
        }
    }
}
﻿using Skeleton.Signals;
using UnityEngine;
using Zenject;

namespace Skeleton
{
    public class UI_Requester : MonoBehaviour
    {
        private Transform _menu;

        [Inject] private SignalBus signalBus;

        private Transform menu
        {
            get
            {
                if (_menu == null)
                {
                    _menu = transform.find<Transform>("Menu");
                }

                return _menu;
            }
        }

        [Inject]
        private void Register()
        {
            signalBus.Subscribe(delegate(PlaybackSignal _signal)
            {
                DetermineAndDecideOnPlaybackStatus(_signal.shouldPlay);
            });
        }

        private void Start()
        {
            StartCoroutine(WaitTime.WaitFrame(delegate
            {
                ShowMenu(false);
            }));
        }

        private void DetermineAndDecideOnPlaybackStatus(bool _isInPlaybackMode)
        {
            ShowMenu(_isInPlaybackMode);
        }


        /// <summary>
        ///     can be accessed by test element
        /// </summary>
        /// <param name="_shouldShowMenu"></param>
        public void ShowMenu(bool _shouldShowMenu)
        {
            menu.gameObject.SetActive(_shouldShowMenu);
        }

        private void OnDestroy()
        {
            UnSubscribe();
        }

        public void UnSubscribe()
        {
            signalBus.TryUnsubscribe(delegate(PlaybackSignal _signal)
            {
                DetermineAndDecideOnPlaybackStatus(_signal.shouldPlay);
            });
        }
    }
}
﻿using System;

namespace Skeleton.Signals
{
	/// <summary>
	///     timeline button has been tapped, so dispatch signal to all directors with ChapterIndicator enums
	///     if the enum corresponds, director plays
	/// </summary>
	public class TimelineDirectorOnCallSignal
    {
        public TimelineIndicator timelineIndicator;
    }


	/// <summary>
	///     tape player button has been tapped, so dispatch signal to currently playing director to play, rewind, stop, pause
	/// </summary>
	public class TapeButtonPlaybackSignal
    {
        public TapePlayerButtonIndicator tapePlayerButtonIndicator;
    }

	/// <summary>
	///     dispatches a signal when any interaction events have happened to the button, where an animator is listening
	/// </summary>
	public class TapeButtonAnimatorSignal
    {
        public TapePlayerButtonIndicator tapePlayerButtonIndicator;
        public int whichState;
    }

//	/// <summary>
//	/// this signal dispatches a button highlighted to the animator in correspondence of its timeline indicator enum
//	/// </summary>
    public class TimelineButtonSignal
    {
        public int whichAnimationState;
        public TimelineIndicator whichTimelineIndicator;
    }


	/// <summary>
	///     change from playback mode to timeline mode
	/// </summary>
	/// `
	public class PlaybackSignal
    {
        public bool shouldPlay;
    }


	/// <summary>
	///     toggles options object on or off
	/// </summary>
	public class ToggleOptionsSignal
    {
        public bool shouldOpenOptions;
    }

	/// <summary>
	///     see scenenames prefab and sceneloadmanager
	/// </summary>
	public class LoadRequestedSceneSignal
    {
        public Action callback;
        public SceneNames sceneNames;
        public bool shouldLoad;
    }


	/// <summary>
	///     restarts the app
	/// </summary>
	public class RestartSignal
    {
    }

	/// <summary>
	///     finishing a timeline on enable
	/// </summary>
	public class CompletedTimelineSignal
    {
        public TimelineIndicator timelineIndicator;
    }

    public class QuitAppSignal
    {
    }
	
	public class AllScenesLoaded
	{
		public bool _scenes_loaded;	
	}


    public class StopAllTimelinesSignal
    {
    }
}
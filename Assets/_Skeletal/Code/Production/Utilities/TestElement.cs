﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class TestElement : MonoBehaviour
{
    private void Awake()
    {
        CheckSceneCount();
    }

    private void CheckSceneCount()
    {
        if (SceneManager.sceneCount > 1)
        {
            DestroyImmediate(gameObject);
        }
    }
}
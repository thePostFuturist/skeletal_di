﻿using System;
using System.Collections.Generic;
using Skeleton.Signals;
using UnityEngine;
using UnityEngine.Networking;
using Zenject;

namespace Skeleton
{
    public class TimelinePrincipal : NetworkBehaviour
    {
        private TimelineDirectorOnCallSignal timelineDirectorOnCallSignal;

        private int timeline_indicator_length;

        [SerializeField] private float timeline_wait_timeout = 10f;

        [Inject] public TimelinesPlayed timelinesPlayed;

        [Inject] private TimelineIndicator nextUpTimeline;
        [Inject] private SignalBus signalBus;

        [Inject]
        private void Subscribe()
        {
            signalBus.Subscribe(delegate(CompletedTimelineSignal _signal)
            {
                OnCompletedTimeline(_signal.timelineIndicator);
            });

            signalBus.Subscribe(delegate(TimelineDirectorOnCallSignal _signal)
            {
                OnCompletedTimeline(_signal.timelineIndicator);
            });

            signalBus.Subscribe(delegate(RestartSignal _signal)
            {
                RestartApp();
            });
        }

        private void Start()
        {
            timeline_indicator_length = Enum.GetNames(typeof(TimelineIndicator)).Length;
            CreateTimelinesPlayedList();
        }

        private void CreateTimelinesPlayedList()
        {
            for (int i = 0; i < timeline_indicator_length; i++)
            {
                timelinesPlayed.timelines.Add(false);
            }
        }

        private void OnCompletedTimeline(TimelineIndicator _timelineIndicator)
        {
            Debug.Log(_timelineIndicator + " finished");
            AssignPlayedState(_timelineIndicator);

            if (_timelineIndicator == TimelineIndicator.Credits)
            {
                RestartApp();
                StartCoroutine(WaitTime.Wait(DetermineWhichTimelineAction, 4));
            }
            else
            {
                TimeoutTimelinesPlayback(true);
            }
        }

        /// <summary>
        ///     iterates through the timelines list, when the member matches the timelines,
        ///     the bool gets assigned to true
        /// </summary>
        /// <param name="_timelineIndicator"></param>
        private void AssignPlayedState(TimelineIndicator _timelineIndicator)
        {
            for (int i = 0; i < timeline_indicator_length; i++)
            {
                if (
                    (TimelineIndicator) i == _timelineIndicator
                )
                {
                    timelinesPlayed.timelines[i] = true;
                }
            }
        }

        /// <summary>
        ///     Waits a specified amount of time, then begins on whichever timeline has not been played
        /// </summary>
        /// <param name="is_waiting"></param>
        private void TimeoutTimelinesPlayback(bool is_waiting)
        {
            if (is_waiting)
            {
                StartCoroutine(WaitTime.Wait(DetermineWhichTimelineAction, timeline_wait_timeout));
            }
            else
            {
                StopAllCoroutines();
            }
        }

        private void DetermineWhichTimelineAction()
        {
            nextUpTimeline = DetermineTimelineToPlay();

            DispatchPlayback(true);
            DispatchTimelineAnimation(nextUpTimeline);
        }

        private void DispatchPlayback(bool state)
        {
            signalBus.Fire(new PlaybackSignal
            {
                shouldPlay = state
            });
        }


        /// <summary>
        ///     iterates through timelines list, if any timeline is marked as false
        ///     returns it
        /// </summary>
        /// <returns></returns>
        private TimelineIndicator DetermineTimelineToPlay()
        {
            for (int i = 0; i < timeline_indicator_length; i++)
            {
                if (timelinesPlayed.timelines[i] == false)
                {
                    return (TimelineIndicator) i;
                }
            }

            // this will never happen, just here so the compiler wont complain of unresolved values
            return TimelineIndicator.Credits;
        }


        private void DispatchTimelineAnimation(TimelineIndicator _timelineIndicator)
        {
            Debug.Log(_timelineIndicator + " dispatching" + this);
            signalBus.Subscribe(delegate(TimelineDirectorOnCallSignal _signal)
            {
                _timelineIndicator = _signal.timelineIndicator;
            });
        }


        /// <summary>
        ///     if a user taps on an organ during timeout, stop it
        ///     because the class is a listener as well as a dispatcher
        ///     this will serve a dual purpose of stopping the timer
        /// </summary>
        /// <param name="_timelineIndicator"></param>
        private void DisruptTimeout(TimelineIndicator _timelineIndicator)
        {
            if (nextUpTimeline != _timelineIndicator)
            {
                TimeoutTimelinesPlayback(false);
            }
        }

        private void DispatchRestartSignal()
        {
            signalBus.Fire(new RestartSignal());
        }

        private void DispatchStopAllTimelinesSignal()
        {
            signalBus.Fire(new StopAllTimelinesSignal());
        }

        /// <summary>
        ///     acts the same way as the stop button on the tape player, so just reusing the command
        /// </summary>
        private void DispatchRewindAllTimelines()
        {
            signalBus.Fire(new TapeButtonPlaybackSignal
            {
                tapePlayerButtonIndicator = TapePlayerButtonIndicator.Stop
            });
        }

        private void SetAllTimelinesViewedToFalse()
        {
            for (int i = 0; i < timeline_indicator_length; i++)
            {
                timelinesPlayed.timelines[i] = false;
            }
        }

        /// <summary>
        ///     rewinds all timelines,
        ///     fades out
        /// </summary>
        private void RestartApp()
        {
            SetAllTimelinesViewedToFalse();

            StartCoroutine(WaitTime.Wait(2f, DispatchStopAllTimelinesSignal));
        }
    }

    [Serializable]
    public class TimelinesPlayed
    {
        [SerializeField] public List<bool> timelines = new List<bool>();
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using Skeleton.Signals;
using UnityEngine;
using Zenject;

namespace Skeleton
{
	public class AppControl : MonoBehaviour {

//		[Inject]
//		Signals.QuitAppSignal quit_app_signal;

		[Inject]
		SignalBus signalBus;
		
		[Inject]
		void Subscribe()
		{
			signalBus.Subscribe<QuitAppSignal>(delegate(QuitAppSignal _signal)
			{
				OnQuitApp();
			});
			
		}

		void OnQuitApp()
		{
			Application.Quit();
		}

	}
}
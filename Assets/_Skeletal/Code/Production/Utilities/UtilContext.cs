using Skeleton.Signals;
using UnityEngine;
using Zenject;

namespace Skeleton
{
	public class UtilContext : MonoInstaller<UtilContext>
	{
		public override void InstallBindings()
		{
			Container.DeclareSignal<Signals.QuitAppSignal>();
		}
	}
}